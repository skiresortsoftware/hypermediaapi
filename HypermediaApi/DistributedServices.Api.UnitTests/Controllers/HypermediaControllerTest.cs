﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Services.Hypermedia;
using Moq;
using System.Collections.Generic;
using DistributedServices.Api.Controllers;
using DistributedServices.Entities;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Caching;

namespace DistributedServices.Api.UnitTests.Controllers
{
    [TestClass]
    public class HypermediaControllerTest
    {
        #region Fields

        private readonly Mock<IMapper<Domain.Entities.Hypermedia, HypermediaDto>> _mockMapper;

        private readonly Mock<ICache<HypermediaDto>> _mockCache;

        private readonly Mock<IHypermediaService> _mockHypermediaService;

        private Domain.Entities.Hypermedia _hypermedia = new Domain.Entities.Hypermedia()
            {
                ClientToken = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Description = "Test Description",
                Href = "http://www.google.com",
                Id = 1,
                Object = "Test",
                ObjectId = 1,
                Target = "_blank",
                Text = "Test Text"
            };

        private HypermediaDto _hypermediaDto = new HypermediaDto()
        {
            ClientToken = Guid.NewGuid().ToString(),
            Description = "Test Description",
            Href = "http://www.google.com",
            Id = 1,
            Object = "Test",
            ObjectId = 1,
            Target = "_blank",
            Text = "Test Text"
        };

        private List<Domain.Entities.Hypermedia> _hypermedias = new List<Domain.Entities.Hypermedia>()
        {
            new Domain.Entities.Hypermedia 
            {
                ClientToken = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Description = "Test Description",
                Href = "http://www.google.com",
                Id = 1,
                Object = "Test",
                ObjectId = 1,
                Target = "_blank",
                Text = "Test Text"
            },
            new Domain.Entities.Hypermedia 
            {
                ClientToken = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Description = "Test Description2",
                Href = "http://www.google2.com",
                Id = 12,
                Object = "Test2",
                ObjectId = 12,
                Target = "_blank",
                Text = "Test Text2"
            }
        };

        private List<HypermediaDto> _hypermediaDtos = new List<HypermediaDto>()
        {
            new HypermediaDto 
            {
                ClientToken = Guid.NewGuid().ToString(),
                Description = "Test Description",
                Href = "http://www.google.com",
                Id = 1,
                Object = "Test",
                ObjectId = 1,
                Target = "_blank",
                Text = "Test Text"
            },
            new HypermediaDto 
            {
                ClientToken = Guid.NewGuid().ToString(),
                Description = "Test Description2",
                Href = "http://www.google2.com",
                Id = 12,
                Object = "Test2",
                ObjectId = 12,
                Target = "_blank",
                Text = "Test Text2"
            }
        };

        #endregion

        #region Controllers

        public HypermediaControllerTest()
        {
            _mockHypermediaService = new Mock<IHypermediaService>();

            _mockMapper = new Mock<IMapper<Domain.Entities.Hypermedia, HypermediaDto>>();

            _mockMapper.Setup(m => m.Map(It.IsAny<HypermediaDto>())).Returns(_hypermedia);

            _mockMapper.Setup(m => m.Map(It.IsAny<Domain.Entities.Hypermedia>())).Returns(_hypermediaDto);

            _mockMapper.Setup(m => m.Map(It.IsAny<List<HypermediaDto>>())).Returns(_hypermedias);

            _mockMapper.Setup(m => m.Map(It.IsAny<List<Domain.Entities.Hypermedia>>())).Returns(_hypermediaDtos);

            _mockCache = new Mock<ICache<HypermediaDto>>();

            _mockHypermediaService.Setup(m => m.GetBy(It.IsAny<Func<Domain.Entities.Hypermedia, bool>>())).Returns(_hypermedia);

            _mockHypermediaService.Setup(m => m.Add(It.IsAny<Domain.Entities.Hypermedia>())).Returns(_hypermedia);

            _mockHypermediaService.Setup(m => m.Update(It.IsAny<Domain.Entities.Hypermedia>())).Returns(_hypermedia);

            _mockHypermediaService.Setup(m => m.Delete(It.IsAny<int>())).Returns(_hypermedia);

            _mockHypermediaService.Setup(m => m.GetManyBy(It.IsAny<Func<Domain.Entities.Hypermedia, bool>>())).Returns(_hypermedias);                  
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void GetAllBy_WithClientTokenAndObj_ReturnsHypermediaDtos()
        {
            // Arrange
            var clientToken = Guid.NewGuid().ToString();

            var obj = "Test";

            var hypermediaController = new HypermediaController(_mockHypermediaService.Object, _mockMapper.Object, _mockCache.Object);

            // Act
            var getAllByResponse = hypermediaController.GetAllBy(clientToken, obj);

            // Assert
            Assert.IsInstanceOfType(getAllByResponse, typeof(List<HypermediaDto>));
        }

        [TestMethod]
        public void GetBy_WithClientTokenObjAndId_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = Guid.NewGuid().ToString();

            var obj = "Test";

            var id = 1;

            var hypermediaController = new HypermediaController(_mockHypermediaService.Object, _mockMapper.Object, _mockCache.Object);

            // Act
            var getByResponse = hypermediaController.GetBy(clientToken, obj, id);

            // Assert
            Assert.IsInstanceOfType(getByResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Post_WithHypermediaDto_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = Guid.NewGuid().ToString();

            var hypermediaController = new HypermediaController(_mockHypermediaService.Object, _mockMapper.Object, _mockCache.Object);

            // Act
            var postResponse = hypermediaController.Post(_hypermediaDto, clientToken);

            // Assert
            Assert.IsInstanceOfType(postResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Put_WithHypermediaDto_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = Guid.NewGuid().ToString();

            var id = 1;

            var hypermediaController = new HypermediaController(_mockHypermediaService.Object, _mockMapper.Object, _mockCache.Object);

            // Act
            var putResponse = hypermediaController.Put(clientToken, id, _hypermediaDto);

            // Assert
            Assert.IsInstanceOfType(putResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Delete_WithId_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = Guid.NewGuid().ToString();

            var id = 1;

            var hypermediaController = new HypermediaController(_mockHypermediaService.Object, _mockMapper.Object, _mockCache.Object);

            // Act
            var deleteResponse = hypermediaController.Delete(clientToken, id);

            // Assert
            Assert.IsInstanceOfType(deleteResponse, typeof(HypermediaDto));
        }

        #endregion
    }
}
