﻿using Application.Services.Hypermedia;
using DistributedServices.Entities;
using Infrastructure.Common.Caching;
using Infrastructure.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DistributedServices.Api.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api")]
    public class HypermediaController : ApiController
    {
        private readonly IHypermediaService _hypermediaService;

        private readonly IMapper<Domain.Entities.Hypermedia, DistributedServices.Entities.HypermediaDto> _mapper;

        private readonly ICache<DistributedServices.Entities.HypermediaDto> _cache;

        private const string HypermediaCacheIdFormat = "hypermedia-{0}-{1}-{2}";

        public HypermediaController(IHypermediaService hypermediaService, IMapper<Domain.Entities.Hypermedia, DistributedServices.Entities.HypermediaDto> mapper, ICache<DistributedServices.Entities.HypermediaDto> cache)
        {
            _hypermediaService = hypermediaService;

            _mapper = mapper;

            _cache = cache;
        }

        [Route("hypermedia/{obj}")]
        public List<HypermediaDto> GetAllBy(string clientToken, string obj)
        {
            var items = _hypermediaService.GetManyBy(i => i.ClientToken == Guid.Parse(clientToken) && i.Object == obj);

            return _mapper.Map(items);
        }

        [Route("hypermedia/{obj}/{id}")]
        public HypermediaDto GetBy(string clientToken, string obj, int id)
        {
            var item = _cache.Get(string.Format(HypermediaCacheIdFormat, clientToken, obj, id));

            if (item != null)
                return item;

            item = _mapper.Map(_hypermediaService.GetBy(i => i.ClientToken == Guid.Parse(clientToken) && i.Object == obj && i.ObjectId == id));

            return item;
        }

        [Route("hypermedia")]
        public HypermediaDto Post(HypermediaDto hypermedia, string clientToken)
        {
            var mappedItem = _mapper.Map(hypermedia);

            var addedItem = _hypermediaService.Add(mappedItem);

            var mappedAddedItem = _mapper.Map(addedItem);

            _cache.Add(mappedAddedItem, string.Format(HypermediaCacheIdFormat, clientToken, addedItem.Object, addedItem.Id));

            return mappedAddedItem;
        }

        [Route("hypermedia/{id}")]
        public HypermediaDto Put(string clientToken, int id, HypermediaDto item)
        {
            item.Id = id;

            var mappedItem = _mapper.Map(item);

            var updatedItem = _hypermediaService.Update(mappedItem);

            var mappedUpdatedItem = _mapper.Map(updatedItem);

            _cache.Update(mappedUpdatedItem, string.Format(HypermediaCacheIdFormat, clientToken, mappedUpdatedItem.Object, mappedUpdatedItem.Id));

            return mappedUpdatedItem;
        }

        [Route("hypermedia/{id}")]
        public HypermediaDto Delete(string clientToken, int id)
        {
            var deletedItem = _hypermediaService.Delete(id);

            _cache.Remove(string.Format(HypermediaCacheIdFormat, clientToken, deletedItem.Object, deletedItem.Id));

            return _mapper.Map(deletedItem);
        }
    }
}
