using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainModule.Models.Mapping
{
    public class HypermediaMap : EntityTypeConfiguration<Domain.Entities.Hypermedia>
    {
        public HypermediaMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Href)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Target)
                .HasMaxLength(20);

            this.Property(t => t.Text)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.Object)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Hypermedia");
            this.Property(t => t.Href).HasColumnName("Href");
            this.Property(t => t.Target).HasColumnName("Target");
            this.Property(t => t.Text).HasColumnName("Text");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Object).HasColumnName("Object");
            this.Property(t => t.ObjectId).HasColumnName("ObjectId");
            this.Property(t => t.ClientToken).HasColumnName("ClientToken");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateModified).HasColumnName("DateModified");
            this.Property(t => t.Id).HasColumnName("Id");
        }
    }
}
