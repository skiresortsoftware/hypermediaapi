using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Infrastructure.Data.MainModule.Models.Mapping;

namespace Infrastructure.Data.MainModule.Models
{
    public partial class HypermediaContext : DbContext
    {
        static HypermediaContext()
        {
            Database.SetInitializer<HypermediaContext>(null);
        }

        public HypermediaContext()
            : base("Name=HypermediaContext")
        {
        }

        public DbSet<Domain.Entities.Hypermedia> Hypermedias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new HypermediaMap());
        }
    }
}
