﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.MainModule.Hypermedia
{
    public interface IHypermediaRepository : IRepository<Domain.Entities.Hypermedia>
    {
    }
}
