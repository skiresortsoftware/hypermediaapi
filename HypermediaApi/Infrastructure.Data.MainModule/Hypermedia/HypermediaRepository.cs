﻿using Infrastructure.Common.Caching;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Hypermedia
{
    public class HypermediaRepository : IHypermediaRepository
    {
        private readonly HypermediaContext _context = new HypermediaContext();

        public List<Domain.Entities.Hypermedia> GetManyBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            var items = _context.Hypermedias.Where(predicate);

            return items.ToList();
        }

        public Domain.Entities.Hypermedia GetBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            var item = _context.Hypermedias.FirstOrDefault(predicate);

            return item;
        }

        public Domain.Entities.Hypermedia Add(Domain.Entities.Hypermedia item)
        {
            item.DateCreated = DateTime.Now;

            var addeditem = _context.Hypermedias.Add(item);

            _context.SaveChanges();

            return addeditem;
        }

        public Domain.Entities.Hypermedia Update(Domain.Entities.Hypermedia item)
        {
            var itemToUpdate = _context.Hypermedias.FirstOrDefault(i => i.Id == item.Id);

            itemToUpdate.Href = item.Href;
            
            itemToUpdate.Description = item.Description;
            
            itemToUpdate.Object = item.Object;
            
            itemToUpdate.ObjectId = item.ObjectId;
            
            itemToUpdate.Text = item.Text;
            
            itemToUpdate.DateModified = DateTime.Now;

            _context.SaveChanges();

            return itemToUpdate;
        }

        public Domain.Entities.Hypermedia Delete(int id)
        {
            var itemToRemove = _context.Hypermedias.FirstOrDefault(i => i.Id == id);

            var deletedItem = _context.Hypermedias.Remove(itemToRemove);

            _context.SaveChanges();

            return itemToRemove;
        }
    }
}
