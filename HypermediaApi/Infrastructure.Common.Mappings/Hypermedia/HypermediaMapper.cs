﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common.Mappings.Hypermedia
{
    public class HypermediaMapper : IMapper<Domain.Entities.Hypermedia, DistributedServices.Entities.HypermediaDto>
    {
        public Domain.Entities.Hypermedia Map(DistributedServices.Entities.HypermediaDto obj)
        {
            if (obj == null)
                return new Domain.Entities.Hypermedia();

            return new Domain.Entities.Hypermedia
            {
                ClientToken = Guid.Parse(obj.ClientToken),
                Description = obj.Description,
                Href = obj.Href,
                Id = obj.Id,
                Object = obj.Object,
                ObjectId = obj.ObjectId,
                Target = obj.Target,
                Text = obj.Text
            };
        }

        public DistributedServices.Entities.HypermediaDto Map(Domain.Entities.Hypermedia obj)
        {
            if (obj == null)
                return new DistributedServices.Entities.HypermediaDto();

            return new DistributedServices.Entities.HypermediaDto
            {
                ClientToken = obj.ClientToken.ToString(),
                Description = obj.Description,
                Href = obj.Href,
                Id = obj.Id,
                Object = obj.Object,
                ObjectId = obj.ObjectId,
                Target = obj.Target,
                Text = obj.Text
            };
        }


        public List<Domain.Entities.Hypermedia> Map(List<DistributedServices.Entities.HypermediaDto> objs)
        {
            if (objs == null)
                return new List<Domain.Entities.Hypermedia>();

            var items = objs.Select(obj => new Domain.Entities.Hypermedia
            {
                ClientToken = Guid.Parse(obj.ClientToken),
                Description = obj.Description,
                Href = obj.Href,
                Id = obj.Id,
                Object = obj.Object,
                ObjectId = obj.ObjectId,
                Target = obj.Target,
                Text = obj.Text
            });

            return items.ToList();
        }

        public List<DistributedServices.Entities.HypermediaDto> Map(List<Domain.Entities.Hypermedia> objs)
        {
            if (objs == null)
                return new List<DistributedServices.Entities.HypermediaDto>();

            var items = objs.Select(obj => new DistributedServices.Entities.HypermediaDto
            {
                ClientToken = obj.ClientToken.ToString(),
                Description = obj.Description,
                Href = obj.Href,
                Id = obj.Id,
                Object = obj.Object,
                ObjectId = obj.ObjectId,
                Target = obj.Target,
                Text = obj.Text
            });

            return items.ToList();
        }
    }
}
