﻿using Infrastructure.Data.MainModule.Hypermedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Mocks.Hypermedia
{
    public class MockHypermediaRepository : IHypermediaRepository
    {
        private static List<Domain.Entities.Hypermedia> hypermedias = new List<Domain.Entities.Hypermedia>
            {
                new Domain.Entities.Hypermedia 
                {
                    ClientToken = Guid.NewGuid(),
                    DateCreated = DateTime.Now,
                    Description = "Test Description1",
                    Href = "http://www.google.com",
                    Id = 1,
                    Object = "TestObject1",
                    ObjectId = 2,
                    Target = "_blank",
                    Text = "Test Text1"
                },
                new Domain.Entities.Hypermedia 
                {
                    ClientToken = Guid.NewGuid(),
                    DateCreated = DateTime.Now,
                    Description = "Test Description2",
                    Href = "http://www.google.com",
                    Id = 2,
                    Object = "TestObject2",
                    ObjectId = 3,
                    Target = "_blank",
                    Text = "Test Text2"
                }
            };

        public List<Domain.Entities.Hypermedia> GetManyBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            return hypermedias;
        }

        public Domain.Entities.Hypermedia GetBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            return hypermedias.First();
        }

        public Domain.Entities.Hypermedia Add(Domain.Entities.Hypermedia item)
        {
            hypermedias.Add(item);

            return item;
        }

        public Domain.Entities.Hypermedia Update(Domain.Entities.Hypermedia item)
        {
            var hypermediaToUpdate = hypermedias.FirstOrDefault(i => i.Id == item.Id);

            hypermedias.Remove(hypermediaToUpdate);

            hypermedias.Add(item);

            return item;
        }

        public Domain.Entities.Hypermedia Delete(int id)
        {
            var hypermediaToRemove = hypermedias.FirstOrDefault(i => i.Id == id);

            hypermedias.Remove(hypermediaToRemove);

            return hypermediaToRemove;
        }
    }
}
