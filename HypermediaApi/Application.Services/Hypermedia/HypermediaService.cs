﻿using DistributedServices.Entities;
using Infrastructure.Data.MainModule.Hypermedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Hypermedia
{
    public class HypermediaService : IHypermediaService
    {
        private readonly IHypermediaRepository _hypermediaRepository;

        public HypermediaService(IHypermediaRepository hypermediaRepository)
        {
            _hypermediaRepository = hypermediaRepository;
        }

        public Domain.Entities.Hypermedia GetBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            var item = _hypermediaRepository.GetBy(predicate);

            return item;
        }

        public Domain.Entities.Hypermedia Add(Domain.Entities.Hypermedia item)
        {
            var addedItem = _hypermediaRepository.Add(item);

            return addedItem;
        }

        public Domain.Entities.Hypermedia Update(Domain.Entities.Hypermedia item)
        {
            var updatedItem = _hypermediaRepository.Update(item);

            return updatedItem;
        }

        public Domain.Entities.Hypermedia Delete(int id)
        {
            var deletedItem = _hypermediaRepository.Delete(id);

            return deletedItem;
        }

        public List<Domain.Entities.Hypermedia> GetManyBy(Func<Domain.Entities.Hypermedia, bool> predicate)
        {
            var items = _hypermediaRepository.GetManyBy(predicate);

            return items;
        }
    }
}
