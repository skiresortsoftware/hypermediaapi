﻿using DistributedServices.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Hypermedia
{
    public interface IHypermediaService : IService<Domain.Entities.Hypermedia>
    {
    }
}
