﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DistributedServices.Api.Controllers;
using DistributedServices.Entities;
using System.Collections.Generic;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Caching;
using Application.Services.Hypermedia;
using Infrastructure.Data.MainModule.Hypermedia;
using Infrastructure.Common.Mappings.Hypermedia;
using Infrastructure.Common.Configuration;

namespace DistributedServices.Api.IntegrationTests.Controllers
{
    [TestClass]
    public class HypermediaControllerTest
    {
        #region Fields

        private readonly IMapper<Domain.Entities.Hypermedia, HypermediaDto> _mapper;

        private readonly ICache<HypermediaDto> _cache;

        private readonly ICacheConfiguration _cacheConfiguration;

        private readonly IHypermediaService _hypermediaService;

        private readonly IHypermediaRepository _hypermediaRepository;

        private Domain.Entities.Hypermedia _hypermedia = new Domain.Entities.Hypermedia()
        {
            ClientToken = Guid.NewGuid(),
            DateCreated = DateTime.Now,
            Description = "Test Description",
            Href = "http://www.google.com",
            Id = 1,
            Object = "Test",
            ObjectId = 1,
            Target = "_blank",
            Text = "Test Text"
        };

        private HypermediaDto _hypermediaDto = new HypermediaDto()
        {
            ClientToken = Guid.NewGuid().ToString(),
            Description = "Test Description",
            Href = "http://www.google.com",
            Id = 1,
            Object = "Test",
            ObjectId = 1,
            Target = "_blank",
            Text = "Test Text"
        };

        private List<Domain.Entities.Hypermedia> _hypermedias = new List<Domain.Entities.Hypermedia>()
        {
            new Domain.Entities.Hypermedia 
            {
                ClientToken = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Description = "Test Description",
                Href = "http://www.google.com",
                Id = 1,
                Object = "Test",
                ObjectId = 1,
                Target = "_blank",
                Text = "Test Text"
            },
            new Domain.Entities.Hypermedia 
            {
                ClientToken = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Description = "Test Description2",
                Href = "http://www.google2.com",
                Id = 12,
                Object = "Test2",
                ObjectId = 12,
                Target = "_blank",
                Text = "Test Text2"
            }
        };

        private List<HypermediaDto> _hypermediaDtos = new List<HypermediaDto>()
        {
            new HypermediaDto 
            {
                ClientToken = Guid.NewGuid().ToString(),
                Description = "Test Description",
                Href = "http://www.google.com",
                Id = 1,
                Object = "Test",
                ObjectId = 1,
                Target = "_blank",
                Text = "Test Text"
            },
            new HypermediaDto 
            {
                ClientToken = Guid.NewGuid().ToString(),
                Description = "Test Description2",
                Href = "http://www.google2.com",
                Id = 12,
                Object = "Test2",
                ObjectId = 12,
                Target = "_blank",
                Text = "Test Text2"
            }
        };

        #endregion

        #region Controllers

        public HypermediaControllerTest()
        {
            _hypermediaRepository = new HypermediaRepository();

            _hypermediaService = new HypermediaService(_hypermediaRepository);

            _mapper = new HypermediaMapper();

            _cacheConfiguration = new CacheConfiguration();

            _cache = new MemoryCache<HypermediaDto>(_cacheConfiguration);
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void GetAllBy_WithClientTokenAndObj_ReturnsHypermediaDtos()
        {
            // Arrange
            var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

            var obj = "Test";

            var hypermediaController = new HypermediaController(_hypermediaService, _mapper, _cache);

            // Act
            var getAllByResponse = hypermediaController.GetAllBy(clientToken, obj);

            // Assert
            Assert.IsInstanceOfType(getAllByResponse, typeof(List<HypermediaDto>));
        }

        [TestMethod]
        public void GetBy_WithClientTokenObjAndId_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

            var obj = "Test";

            var id = 2;

            var hypermediaController = new HypermediaController(_hypermediaService, _mapper, _cache);

            // Act
            var getByResponse = hypermediaController.GetBy(clientToken, obj, id);

            // Assert
            Assert.IsInstanceOfType(getByResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Post_WithHypermediaDto_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

            var hypermediaController = new HypermediaController(_hypermediaService, _mapper, _cache);

            // Act
            var postResponse = hypermediaController.Post(_hypermediaDto, clientToken);

            // Assert
            Assert.IsInstanceOfType(postResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Put_WithHypermediaDto_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

            var id = 2;

            var hypermediaController = new HypermediaController(_hypermediaService, _mapper, _cache);

            // Act
            var putResponse = hypermediaController.Put(clientToken, id, _hypermediaDto);

            // Assert
            Assert.IsInstanceOfType(putResponse, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Delete_WithId_ReturnsHypermediaDto()
        {
            // Arrange
            var clientToken = "8384DDA6-371E-40A3-B343-4D061B00B52C";

            var id = 2;

            var hypermediaController = new HypermediaController(_hypermediaService, _mapper, _cache);

            // Act
            var deleteResponse = hypermediaController.Delete(clientToken, id);

            // Assert
            Assert.IsInstanceOfType(deleteResponse, typeof(HypermediaDto));
        }

        #endregion
    }
}
