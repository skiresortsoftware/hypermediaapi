﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Hypermedia
    {
        public string Href { get; set; }
        public string Target { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Object { get; set; }
        public int ObjectId { get; set; }
        public Guid ClientToken { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public int Id { get; set; }
    }
}
