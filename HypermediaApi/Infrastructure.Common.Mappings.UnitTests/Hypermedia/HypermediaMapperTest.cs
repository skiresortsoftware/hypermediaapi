﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infrastructure.Common.Mappings.Hypermedia;
using DistributedServices.Entities;
using System.Collections.Generic;

namespace Infrastructure.Common.Mappings.UnitTests.Hypermedia
{
    [TestClass]
    public class HypermediaMapperTest
    {
        #region Fields

        private readonly Domain.Entities.Hypermedia _domainObject = new Domain.Entities.Hypermedia
        {
            ClientToken = Guid.NewGuid(),
            Description = "Test Description1",
            Href = "http://www.google.com",
            Id = 1,
            Object = "TestObject1",
            ObjectId = 2,
            Target = "_blank",
            Text = "Test Text1"
        };

        private readonly HypermediaDto _dtoObject = new HypermediaDto
        {
            ClientToken = Guid.NewGuid().ToString(),
            Description = "Test Description1",
            Href = "http://www.google.com",
            Id = 1,
            Object = "TestObject1",
            ObjectId = 2,
            Target = "_blank",
            Text = "Test Text1"
        };

        private readonly List<Domain.Entities.Hypermedia> _domainObjects = new List<Domain.Entities.Hypermedia>()
        {
                new Domain.Entities.Hypermedia
                {
                    ClientToken = Guid.NewGuid(),
                    Description = "Test Description1",
                    Href = "http://www.google.com",
                    Id = 1,
                    Object = "TestObject1",
                    ObjectId = 2,
                    Target = "_blank",
                    Text = "Test Text1"
                },
                new Domain.Entities.Hypermedia
                {
                    ClientToken = Guid.NewGuid(),
                    Description = "Test Description2",
                    Href = "http://www.google.com",
                    Id = 2,
                    Object = "TestObject2",
                    ObjectId = 3,
                    Target = "_blank",
                    Text = "Test Text2"
                }
            };


        private readonly List<HypermediaDto> _dtoObjects = new List<HypermediaDto>
            {
                new HypermediaDto
                {
                    ClientToken = Guid.NewGuid().ToString(),
                    Description = "Test Description1",
                    Href = "http://www.google.com",
                    Id = 1,
                    Object = "TestObject1",
                    ObjectId = 2,
                    Target = "_blank",
                    Text = "Test Text1"
                },
                new HypermediaDto
                {
                    ClientToken = Guid.NewGuid().ToString(),
                    Description = "Test Description2",
                    Href = "http://www.google.com",
                    Id = 2,
                    Object = "TestObject2",
                    ObjectId = 3,
                    Target = "_blank",
                    Text = "Test Text2"
                }
            };


        #endregion

        #region Test methods

        [TestMethod]
        public void Map_WithDomainObject_ReturnsClientTokenGuid()
        {
            // Arrange
            var mapper = new HypermediaMapper();

            Guid result;

            // Act
            var dtoObject = mapper.Map(_domainObject);

            var isClientTokenGuid = Guid.TryParse(dtoObject.ClientToken, out result);

            // Assert
            Assert.IsTrue(isClientTokenGuid);
        }

        [TestMethod]
        public void Map_WithDtoObject_ReturnsDomainObject()
        {
            // Arrange
            var mapper = new HypermediaMapper();

            // Act
            var domainObject = mapper.Map(_dtoObject);

            // Assert
            Assert.IsInstanceOfType(domainObject, typeof(Domain.Entities.Hypermedia));
        }

        [TestMethod]
        public void Map_WithDtoObjects_ReturnsDomainObjects()
        {
            // Arrange
            var mapper = new HypermediaMapper();

            // Act
            var domainObjects = mapper.Map(_dtoObjects);

            // Assert
            Assert.IsInstanceOfType(domainObjects, typeof(List<Domain.Entities.Hypermedia>));
        }

        [TestMethod]
        public void Map_WithDomainObject_ReturnsDtoObject()
        {
            // Arrange
            var mapper = new HypermediaMapper();

            // Act
            var dtoObject = mapper.Map(_domainObject);

            // Assert
            Assert.IsInstanceOfType(dtoObject, typeof(HypermediaDto));
        }

        [TestMethod]
        public void Map_WithDomainObjects_ReturnsDtoObjects()
        {
            // Arrange
            var mapper = new HypermediaMapper();

            // Act
            var dtoObjects = mapper.Map(_domainObjects);

            // Assert
            Assert.IsInstanceOfType(dtoObjects, typeof(List<HypermediaDto>));
        }

        #endregion

    }
}
